import com.devcamp.shapes.Circle;

public class App {
    public static void main(String[] args) throws Exception {
        Circle circle1 = new Circle();
        Circle circle2 = new Circle(3.0);

        System.out.println(circle1);
        System.out.println(circle2);

        System.out.println("Diện tích hình tròn 1: " + circle1.getArea());
        System.out.println("Chu vi hình tròn 2: " + circle1.getCircumference());

        System.out.println("Diện tích hình tròn 2: " + circle2.getArea());
        System.out.println("Chu vi hình tròn 2: " + circle2.getCircumference());
    }
}
